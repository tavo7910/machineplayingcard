//
//  TAVCard.m
//  MachineToPlaySet
//
//  Created by tavo7910 on 3/26/17.
//  Copyright © 2017 tavo7910. All rights reserved.
//

#import "TAVCard.h"

@implementation TAVCard

-(NSInteger)match:(NSArray *)otherCards {
    NSInteger score = 0;
    for(TAVCard *card in otherCards){
        if([card.contents isEqualToString:self.contents]){
            score=1;
        }
    }
    return score;
}

@end
