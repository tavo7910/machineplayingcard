//
//  ViewController.m
//  MachineToPlaySet
//
//  Created by tavo7910 on 3/24/17.
//  Copyright © 2017 tavo7910. All rights reserved.
//

#import "ViewController.h"
#import "TAVPlayingDeck.h"
#import "TAVMatchingGame.h"

@interface ViewController ()
@property (strong, nonatomic) TAVDeck *deck;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *cardButtons;
@property (strong, nonatomic) TAVMatchingGame *game;
@property (nonatomic) NSInteger countAux;
@property (weak, nonatomic) IBOutlet UILabel *scoreLabel;
- (IBAction)ResetButton:(UIButton *)sender;
@property (nonatomic) NSInteger modeControl;
@end

@implementation ViewController

- (NSInteger)modeControl{
    if(_modeControl ==0) {
        _modeControl =2;
    }
    return _modeControl;
}

- (TAVMatchingGame *)game {
    if(!_game) _game = [[TAVMatchingGame alloc] initWithCount:[self.cardButtons count] usingDeck:[self createDeck]];
    return _game;
    
}

- (TAVDeck *)deck{
    if(!_deck){
        _deck = [self createDeck];
    }
    return _deck;
}

- (TAVDeck *)createDeck{
    return [[TAVPlayingDeck alloc]init];
}

- (IBAction)touchCardButton:(UIButton *)sender {
    long chosenButtonIndex = [self.cardButtons indexOfObject:sender];
    if(self.modeControl==2){
    [self.game chooseCardAtIndex:chosenButtonIndex];
    
    }if(self.modeControl==3){
        [self.game chooseCardAt3:chosenButtonIndex];
}
    
    [self updateUI];
    self.countAux++;
}

- (void)updateUI {
    for(UIButton *cardButton in self.cardButtons) {
        long cardButtonIndex = [self.cardButtons indexOfObject:cardButton];
        TAVCard *card =[self.game cardAtIndex:cardButtonIndex];
        [cardButton setTitle:[self titleForCard:card] forState:UIControlStateNormal];
        [cardButton setBackgroundImage:[self backgraoundImageForCard:card] forState:UIControlStateNormal];
        cardButton.enabled = !card.isMatched;
        self.scoreLabel.text = [NSString stringWithFormat:@"%ld", (long)self.game.score];
    }
    
}

- (NSString *)titleForCard:(TAVCard *) card {
    return card.isChosen ? card.contents : @"";
}

- (UIImage *) backgraoundImageForCard: (TAVCard *)card {
    return [UIImage imageNamed:card.isChosen ? @"cardfront" : @"cardback"];
}

- (IBAction)setLevel:(id)sender {
    
    switch (((UISegmentedControl *)sender).selectedSegmentIndex) {
        case 0:
            self.modeControl = 2;
            break;
            
        case 1:{
            self.modeControl = 3;
//            long chosenButtonIndex = [self.cardButtons indexOfObject:sender];
//            [self.game chooseCardAt3:chosenButtonIndex];
//            [self updateUI];
                }
            break;
            
        default:break;
    }
}

- (IBAction)ResetButton:(UIButton *)sender {
    [self resetUI];
    [self updateUI];
}

- (void)cancelButtonPressed{
}

- (void)resetUI{
    if(_game) _game = [[TAVMatchingGame alloc] initWithCount:[self.cardButtons count] usingDeck:[self createDeck]];
}

@end
