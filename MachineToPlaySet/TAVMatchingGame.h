//
//  TAVMatchingGame.h
//  MachineToPlaySet
//
//  Created by tavo7910 on 3/26/17.
//  Copyright © 2017 tavo7910. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TAVDeck.h"
#import "TAVDeck.h"

@interface TAVMatchingGame : NSObject

- (instancetype)initWithCount: (NSInteger) count usingDeck: (TAVDeck *)deck;
- (void)chooseCardAtIndex:(NSInteger)index;
- (TAVCard *)cardAtIndex:(NSInteger)index;
- (void)chooseCardAt3:(NSInteger)index;

@property (nonatomic, readonly) NSInteger score;
@end
