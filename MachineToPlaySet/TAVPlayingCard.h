//
//  TAVPlayingCard.h
//  MachineToPlaySet
//
//  Created by tavo7910 on 3/26/17.
//  Copyright © 2017 tavo7910. All rights reserved.
//

#import "TAVCard.h"

@interface TAVPlayingCard : TAVCard

@property (strong, nonatomic) NSString *suit;
@property (nonatomic) NSInteger rank;

+ (NSArray *)validSuits;
+ (NSInteger)maxRank;

@end
