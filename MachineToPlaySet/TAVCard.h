//
//  TAVCard.h
//  MachineToPlaySet
//
//  Created by tavo7910 on 3/26/17.
//  Copyright © 2017 tavo7910. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TAVCard : NSObject

@property (strong, nonatomic ) NSString *contents;
@property(nonatomic, getter=isChosen) BOOL chosen; //a form to make an Alias
@property(nonatomic, getter=isMatched) BOOL matched;

- (NSInteger)match:(NSArray *)otherCards;

@end
