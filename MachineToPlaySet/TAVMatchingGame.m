//
//  TAVMatchingGame.m
//  MachineToPlaySet
//
//  Created by tavo7910 on 3/26/17.
//  Copyright © 2017 tavo7910. All rights reserved.
//

#import "TAVMatchingGame.h"

@interface TAVMatchingGame()
@property (nonatomic, readwrite) NSInteger score;
@property (nonatomic, strong) NSMutableArray *cards;
@property (strong, nonatomic) NSMutableArray *mutant;
@end


@implementation TAVMatchingGame

- (NSMutableArray *)cards {
    if (!_cards) _cards = [[NSMutableArray alloc] init];
    return _cards;
}

- (NSMutableArray *)mutant{

    if(!_mutant) _mutant = [[NSMutableArray alloc]init ];
    return _mutant;
}



- (instancetype) initWithCount: (NSInteger) count usingDeck: (TAVDeck *)deck {
    self =[super init];
    if(self){
        for (NSInteger i = 0; i< count; i++) {
            TAVCard *card = [deck drawRandomCard];
            if(card){
                [self.cards addObject:card];
            } else{
                self = nil;
                break;
            }
        }
    }
    return self;
}

- (TAVCard *)cardAtIndex:(NSInteger)index {
    return (index<[self.cards count]) ? self.cards[index] : nil;
}

static const NSInteger MISMATCH_PENALTY =2;
static const NSInteger MATCH_BONUS = 4;
static const NSInteger COST_TO_CHOOSE = 1;


- (void)chooseCardAtIndex:(NSInteger)index {
    TAVCard *card = [self cardAtIndex:index];
    if(!card.isMatched){
        if(card.isChosen){
            card.chosen = NO;
        }  else {
            for(TAVCard *otherCard in self.cards){
                if (otherCard.isChosen && !otherCard.isMatched){
                    NSInteger matchScore= [card match:@[otherCard]];
                    if(matchScore){
                        self.score += matchScore*MATCH_BONUS;
                        otherCard.matched = YES;
                        card.matched = YES;
                        
                    } else {
                        self.score -=MISMATCH_PENALTY;
                        otherCard.chosen = NO;
                    }
                    break;
                }
            }
            self.score -= COST_TO_CHOOSE;
            card.chosen = YES;
        }
        
    }
}

- (void)chooseCardAt3:(NSInteger)index{
    
    TAVCard *card = [self cardAtIndex:index];
    if(!card.isMatched){
        if(card.isChosen){
            card.chosen = NO;
        }  else {
            NSInteger counter=0;
            for(TAVCard *otherCard in self.cards){
                if (otherCard.isChosen && !otherCard.isMatched){
                    counter++;
                    if(counter ==2){
                        NSInteger matchScore= [card match:self.mutant];
                        if(matchScore){
                             self.mutant = nil;
                            card.matched = YES;
                            self.score += matchScore*MATCH_BONUS;
                            for(TAVCard *otherCard in self.cards){
                                if (otherCard.isChosen && !otherCard.isMatched){
                                    otherCard.matched = YES;
                                }
                            }
                        }
                        //no points
                        else{
                            self.score -=MISMATCH_PENALTY;
                            self.score -= COST_TO_CHOOSE;
                            self.mutant = nil;
                            for(TAVCard *otherCard in self.cards){
                                if (otherCard.isChosen && !otherCard.isMatched){
                                    otherCard.chosen = NO;
                                }
                            }
                            return;
                        }
                    }//if mutant.count == 2
                    else {
                        
                       
                    }
                    // break;
                }
            }
          
        }//The array is full is necesary to clean it. If you win it is not necessary to charge to the array
        self.score -= COST_TO_CHOOSE;
          card.chosen = YES;
        if(card.matched){
            self.mutant = nil;
        }else{
         [self.mutant addObject: card];
        }
    }
}


@end
