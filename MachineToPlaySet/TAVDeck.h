//
//  TAVDeck.h
//  MachineToPlaySet
//
//  Created by tavo7910 on 3/26/17.
//  Copyright © 2017 tavo7910. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TAVCard.h"

@interface TAVDeck : NSObject

- (void)addCard:(TAVCard *)card atTop:(BOOL)atTop;
- (void)addCard:(TAVCard *)card;
- (TAVCard *)drawRandomCard;

@end
