//
//  TAVDeck.m
//  MachineToPlaySet
//
//  Created by tavo7910 on 3/26/17.
//  Copyright © 2017 tavo7910. All rights reserved.
//

#import "TAVDeck.h"

@interface TAVDeck()
@property (strong, nonatomic) NSMutableArray *cards;
@end

@implementation TAVDeck

- (NSMutableArray *)cards {
    if(!_cards) _cards = [[NSMutableArray alloc] init];
    return _cards;
}

- (void)addCard:(TAVCard *)card atTop:(BOOL)atTop {
    if(atTop){
        [self.cards insertObject:card atIndex:0];
    }
    else {
        [self.cards addObject:card];
    }
}

- (void)addCard:(TAVCard *)card {
    [self addCard:card atTop:NO];
}

- (TAVCard *)drawRandomCard {
    TAVCard *randomCard = nil;
    if([self.cards count]) {
        unsigned index = arc4random() % [self.cards count];
        randomCard = self.cards[index];
        [self.cards removeObjectAtIndex:index];
    }
    return randomCard;
}

@end
