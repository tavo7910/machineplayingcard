//
//  TAVPlayingDeck.m
//  MachineToPlaySet
//
//  Created by tavo7910 on 3/26/17.
//  Copyright © 2017 tavo7910. All rights reserved.
//

#import "TAVPlayingDeck.h"
#import "TAVPlayingCard.h"

@implementation TAVPlayingDeck

- (instancetype)init {
    self = [super init];
    
    if(self) {
        for(NSString *suit in [TAVPlayingCard validSuits]) {
            for(NSInteger rank = 1; rank<=[TAVPlayingCard maxRank]; rank++) {
                TAVPlayingCard *card = [[TAVPlayingCard alloc] init];
                card.rank = rank;
                card.suit = suit;
                [self addCard:card];
            }
        }
    }
    return self;
}

@end
