//
//  TAVPlayingCard.m
//  MachineToPlaySet
//
//  Created by tavo7910 on 3/26/17.
//  Copyright © 2017 tavo7910. All rights reserved.
//

#import "TAVPlayingCard.h"

@implementation TAVPlayingCard

- (NSInteger)match:(NSArray *)otherCards {
    NSInteger score = 0;
    for(NSInteger i= 0; i<otherCards.count; i++){
        TAVPlayingCard *otherCard = otherCards[i];
        if(otherCard.rank == self.rank){
            score = 4;
        }else if ([otherCard.suit isEqualToString:self.suit]) {
            score = 1;
        }
        
        
        if(i>0){
            TAVPlayingCard *arrayCard = otherCards[i];
            TAVPlayingCard *arrayCardBefore = otherCards[i-1];
            if(arrayCard.rank ==  arrayCardBefore.rank) {
                score = 4;
            }else if ([arrayCard.suit isEqualToString:arrayCardBefore.suit]) {
                score = 1;
            }
        }
        
    }
    return score;
}

- (NSString *)contents {
    NSArray *arrayStrings =[TAVPlayingCard rankStrings];
    return [[arrayStrings objectAtIndex:self.rank] stringByAppendingString:self.suit];
}

@synthesize suit = _suit;

+ (NSArray *)validSuits {
    return @[@"♣︎",@"♥︎",@"♠︎",@"♦︎"];
}

- (void)setSuit:(NSString *)suit {
    if([[TAVPlayingCard validSuits] containsObject:suit]) {
        _suit =suit;
    }
}

- (NSString *)suit {
    return _suit ? _suit: @"?";
}

+ (NSArray *)rankStrings {
    return @[@"?",@"A",@"2",@"3",@"4",@"5",@"6",@"7",@"8",@"9",@"10",@"J",@"Q",@"K"];
    
}

+ (NSInteger)maxRank {
    return [[self rankStrings] count] -1;
}

- (void)setRank:(NSInteger)rank {
    if(rank <= [TAVPlayingCard maxRank] ){
        _rank = rank;
    }
}

@end
